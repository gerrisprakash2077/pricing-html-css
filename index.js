function something() {

    let btn = document.querySelector('.button')
    let basic = document.getElementById("basic-price")
    let professional = document.getElementById("professional-price")
    let master = document.getElementById("master-price")

    if (btn.checked == true) {
        basic.textContent = '$199.99';
        professional.textContent = '$249.99';
        master.textContent = '$399.99';
    }
    else {
        basic.textContent = '$19.99';
        professional.textContent = '$24.99';
        master.textContent = '$39.99';
    }

}
